package com.suhel.contactmanagement.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.io.FileInputStream;

public class PropertyReader
{
	static String mode="";
	public static String getProperty(String configFile,String propKey)throws IOException
	{
		Properties prop=new Properties();
		InputStream input=null;
		
		try
		{
			input=new FileInputStream(configFile);
		// load a properties file
			prop.load(input);
		// get the property value and print it out
			mode = prop.getProperty(propKey);
	
		return mode;
		}
		catch (IOException ex)
		{
			System.out.println("Exception is "+ ex.getMessage());
		} 
		finally 
		{
		if (input != null) 
		{
			try 
			{
			input.close();
			} 
			catch (IOException ex) 
			{
			ex.printStackTrace();
			}
		}
	}
	return mode;
	}
}