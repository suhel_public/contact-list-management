/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suhel.contactmanagement.file;

import java.io.File;

public class FileLayer {
  static InterfaceFileLayer fileObj=null;
  public static InterfaceFileLayer getfileLayer(File file)
          {
             String filename = file.getName().toLowerCase();
              if(filename.endsWith(".txt")){
                fileObj=new TextFile().getFileObj();
              }else if(filename.endsWith(".xls"))
              {
                fileObj=new ExcelFile().getFileObj();
              }
              return fileObj;
          }
}
