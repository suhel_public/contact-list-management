package com.suhel.contactmanagement.file;

import com.suhel.contactmanagement.models.Contacts;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

public class ExcelFile implements InterfaceFileLayer {
    ArrayList<Contacts>contactlist=new ArrayList<Contacts>();
    
    @Override
    public ArrayList<Contacts>getFile(File file)
    {
    try {
            POIFSFileSystem fs = new POIFSFileSystem(file);
            HSSFWorkbook wb = new HSSFWorkbook(fs);

            HSSFSheet sheet = wb.getSheetAt(0);
            Row row;
            for (int i = 0; i <= sheet.getLastRowNum(); i++) {

               row = sheet.getRow(i);
               //int id= (int) row.getCell(0).getNumericCellValue();
               String name = row.getCell(0).getStringCellValue();
               double cell = row.getCell(1).getNumericCellValue();
               String phone=String.valueOf(cell);
               String email = row.getCell(2).getStringCellValue();
               String address= row.getCell(3).getStringCellValue();
               String city = row.getCell(4).getStringCellValue();
               String country = row.getCell(5).getStringCellValue();  
               Contacts contacts=new Contacts(name,phone,email,address,city,country);
               contactlist.add(contacts);
            }
            System.out.println("Success import excel to mysql table");
    }catch (IOException ioe){
        System.out.println(ioe);
    }  
    return contactlist;
    }

    @Override
    public InterfaceFileLayer getFileObj() {
      return this;
    }
}
