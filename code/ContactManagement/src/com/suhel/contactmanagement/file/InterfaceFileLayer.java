/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suhel.contactmanagement.file;

import com.suhel.contactmanagement.models.Contacts;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Md Suhel Rana
 */
public interface InterfaceFileLayer {
    InterfaceFileLayer getFileObj();
    ArrayList<Contacts>getFile(File file);
}
