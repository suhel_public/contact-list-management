package com.suhel.contactmanagement.file;

import com.suhel.contactmanagement.models.Contacts;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TextFile implements InterfaceFileLayer{
    BufferedReader bufferedReader;
    FileReader reader;
    String line=null;
    ArrayList<Contacts>contactlist=new ArrayList<Contacts>();
    @Override
    public ArrayList<Contacts> getFile(File file)
    {                  
       
        try {
            reader = new FileReader(file);
            bufferedReader = new BufferedReader(reader);
            while((line = bufferedReader.readLine()) != null)
          {
            String lineArr[]=line.split("\\$\\%");
            Contacts contacts=new Contacts(lineArr[0],lineArr[1],lineArr[2],lineArr[3],lineArr[4],lineArr[5]);   
            contactlist.add(contacts);
          }
        } catch (FileNotFoundException ex) {
           System.out.println("FileNotFound Exception : "+ ex.getMessage());
        } catch (IOException ex) {
             System.out.println("IO Exception : "+ ex.getMessage());
        }
        
        finally{
         if(bufferedReader!=null)
         {
             try {
                 bufferedReader.close();
             } catch (IOException ex) {
                 ex.getMessage();
             }
         }
        }
        /* always close the file after use */    
      return contactlist; 
    }

    @Override
    public InterfaceFileLayer getFileObj() {
      return this; 
    }
}
