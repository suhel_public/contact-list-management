
package com.suhel.contactmanagement.controllers;

import com.suhel.contactmanagement.models.Contacts;
import com.suhel.contactmanagement.uis.ContactUIController;
import com.suhel.contactmanagement.uis.IContactUi;
import java.io.File;
import java.util.ArrayList;


public interface InterfaceMainController {
    void checkLogin(String userid, String password);
    void addcontact(String name, String phone,String email,String address,String city,String country);
    ArrayList<Contacts> getContacts();
    void setContactUi(IContactUi contactuiController);
    void processFile(File file);
    
}
