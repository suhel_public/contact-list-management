
package com.suhel.contactmanagement.controllers;
import com.suhel.contactmanagement.dal.AbstractDBLayer;
import com.suhel.contactmanagement.file.ExcelFile;
import com.suhel.contactmanagement.file.FileLayer;
import com.suhel.contactmanagement.file.InterfaceFileLayer;
import com.suhel.contactmanagement.file.TextFile;
import com.suhel.contactmanagement.models.Users;
import com.suhel.contactmanagement.models.Contacts;
import com.suhel.contactmanagement.uis.IContactUi;
import com.suhel.contactmanagement.uis.ILoginUi;
import java.io.File;
import java.util.ArrayList;
import org.apache.log4j.Logger;
//import java.util.logging.Logger;


public class MainController implements InterfaceMainController{
      ILoginUi loginUi=null;
      IContactUi contactui=null; 
      AbstractDBLayer dblayer=null;
      InterfaceFileLayer interfaceFileLayer=null;
      private static final Logger LOGGER=Logger.getLogger(MainController.class);
      
    @Override
    public void checkLogin(String userid, String password) {
        LOGGER.debug("is user exists"+userid);
  // connect to the dal and pass on user object to check
    Users user=new Users();
    user.setUserid(userid);
    user.setPassword(password);
    try
    {
        boolean ck=dblayer.userExists(user);
        if(ck)
        {
            LOGGER.info("user exists");
            loginUi.loginSuccess();
        }else
        {
            loginUi.loginFailed();
        }
    }catch(Exception e)
    {
        LOGGER.error("erro is "+e);
        e.printStackTrace();
        loginUi.showErrorMessage(e.getMessage());
    }
   }

    @Override
    public void addcontact(String name, String phone, String email,String address,String city,String country )
    {
 // connect to the dal and pass on contact object to check
       
    Contacts contact=new Contacts();
    contact.setName(name);
    contact.setPhone(phone);
    contact.setEmail(email);
    contact.setAddress(address);
    contact.setCity(city);
    contact.setCountry(country);
    
      try
    {
        System.out.println(contact.getName()+contact.getPhone()+contact.getEmail()+contact.getAddress()+contact.getCity()+contact.getCountry());
        boolean ck=dblayer.checkInsertContact(contact);
        if(ck)
        {
            contactui.addSuccess();
        }else
        {
            contactui.addFailed();
        }
    }catch(Exception e)
    {
        e.printStackTrace();
        contactui.errorMessageSent(e.getMessage());
    }
    }
    
    
     @Override
    public ArrayList<Contacts> getContacts() {
   
         try {
             ArrayList<Contacts>contacts=dblayer.getAllContacts();
             if(contacts!=null){
             return contacts;
             }
         } catch (Exception ex) {
             contactui.errorMessageSent(ex.getMessage());
            System.out.println();
         }
         return null;
    }
    
    
    public void setLoginUi(ILoginUi loginui)
    {
        this.loginUi=loginui;
    }

    @Override
    public void setContactUi(IContactUi contactuiController) {
        this.contactui=contactuiController;
    }
    
      public void setDBLayer(AbstractDBLayer dblayer)
    {
       this.dblayer=dblayer;   
       System.out.println(" set the DBLayer object for initializing");
    }

    @Override
    public void processFile(File file) {   
      interfaceFileLayer= FileLayer.getfileLayer(file);
       
      ArrayList<Contacts> contactlist=interfaceFileLayer.getFile(file);
      for(Contacts contacts:contactlist)
      {
        try
        {
            boolean ck=dblayer.checkInsertContact(contacts);
            if(ck)
             {
            contactui.addSuccess();
             }else
             {
            contactui.addFailed();
             }
         }catch(Exception e)
        {
          e.printStackTrace();
          contactui.errorMessageSent(e.getMessage());
          }
      }     
    }
}
