
package com.suhel.contactmanagement;

import com.suhel.contactmanagement.dal.DBLayerFactory;
import com.suhel.contactmanagement.controllers.MainController;
import com.suhel.contactmanagement.dal.MysqlDBLayer;
import com.suhel.contactmanagement.uis.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import com.suhel.contactmanagement.uis.networks.ServerSide;


public class ApplicationLauncher extends Application{
    MainController maincontroller=null;
    public static Stage stage;
   // DBLayerFactory dblayer=new DBLayerFactory();
      @Override
    public void start(Stage stage) throws IOException, Exception{
        this.stage=stage;
        System.out.println("start the method");
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/com/suhel/contactmanagement/uis/login_ui.fxml"));
       
        System.out.println("Calling schene method");
        Parent root = loader.load();
          
        System.out.println("Calling schene method");
        Scene scene = new Scene(root);  
        stage.setScene(scene);
        //stage.setTitle(STYLESHEET_MODENA);
       
        //----- get object of LoginUiController -----
        LoginUiController loginuiController=
                loader.<LoginUiController>getController();

         this.maincontroller=new MainController();
        System.out.println("calling show method");
        loginuiController.setController(maincontroller);
        maincontroller.setLoginUi(loginuiController);
        try{
            //here is the main problem
        maincontroller.setDBLayer(DBLayerFactory.getDBlayer());
        
        }catch(Exception e)
        {
           System.out.println("error is ......"+e.getMessage());
        }
        //maincontroller.setContactUi(new ContactUIController());
         
        ServerSide server=new ServerSide();
        server.start();
        
        stage.show();
       // server.handleServer();
    }
}
