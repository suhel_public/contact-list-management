
package com.suhel.contactmanagement.dal;

import com.suhel.contactmanagement.utilities.PropertyReader;


public class DBLayerFactory {
   
   static AbstractDBLayer dbname=null; 
   public static AbstractDBLayer getDBlayer()throws Exception
   {
      String db=PropertyReader.getProperty("config.properties", "db");
      Class c=Class.forName(db);
      Object obj=c.newInstance();
      dbname=(AbstractDBLayer) obj;   
      return dbname;
   }
}