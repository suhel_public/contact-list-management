
package com.suhel.contactmanagement.dal;

import com.suhel.contactmanagement.models.Contacts;
import com.suhel.contactmanagement.models.Users;
import com.suhel.contactmanagement.utilities.PropertyReader;
import java.sql.Connection;
import java.sql.DriverManager;


import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


public class MysqlDBLayer extends AbstractDBLayer {
    //those variable are from parent class AbstractDBlayer 
   /* Statement st;
    String dbhost;
    String dbname;
    String dbuser;
    String dbpass;
    String fullconnString="";
    Connection con=null; */
   
     @Override
    public boolean userExists(Users u)throws Exception  {
        
        dbconnect();
             String query="select * from users where user_id='"+u.getUserid()+"'and password='"+u.getPassword()+"';";
             ResultSet rs=st.executeQuery(query);
            
             if(rs.next())
             {
             String userid= rs.getString("user_id");
             String password= rs.getString("password");
             
             if(userid.equals(u.getUserid())&& password.equals(u.getPassword()))
             {
                 System.out.println("login success");
                 return true;
             } 
             }
             closeConn();
             return false;
    }
     @Override
    public boolean checkInsertContact(Contacts contact)throws Exception
    {
        dbconnect();
        String query="insert into contacts values("+contact.getId()+",'"+contact.getName()+"','"+contact.getPhone()+"','"+contact.getEmail()+"','"+contact.getAddress()+"','"+contact.getCity()+"','"+contact.getCountry()+"');";
        int result=st.executeUpdate(query);
        if(result>0)
        {
            return true;
        }
        closeConn();
        return false;
    }
    public boolean checkUpdateContact(String name)
    {
        return true;
    }
     @Override
    public ArrayList<Contacts> getAllContacts()throws Exception
    {
        dbconnect();
         ArrayList<Contacts>contacts=new ArrayList<Contacts>();
         String query="select * from contacts;";
         ResultSet rs=st.executeQuery(query);
         while(rs.next())
         {
             int id=rs.getInt("id");
             String name=rs.getString("name");
             String phone=rs.getString("phone");
             String email=rs.getString("email");
             String address=rs.getString("address");
             String city=rs.getString("city");
             String country=rs.getString("country");
             
             Contacts contact=new Contacts(id,name,phone,email,address,city,country);
             contacts.add(contact);  
         }  
         closeConn();
         return contacts;
    }
    
    
    
    //------ connect to the database -----
     @Override
        public  void dbconnect()
     {
         System.out.println("db connect called");
        try
        {
            //Class.forName("com.mysql.jdbc.Driver");
            dbhost=PropertyReader.getProperty("config.properties", "dbhost");
            dbname=PropertyReader.getProperty("config.properties", "dbname");
            dbuser=PropertyReader.getProperty("config.properties", "dbuser");
            dbpass=PropertyReader.getProperty("config.properties", "dbpass");
            fullconnString="jdbc:mysql://"+dbhost+"/"+dbname;
            con=(Connection) DriverManager.getConnection(fullconnString,dbuser,dbpass);
            st = (Statement) con.createStatement();
        }
        catch(Exception e)
        {
            e.getMessage();
        }
    }
        

    @Override
    public AbstractDBLayer getDBLayer() {
     return this;
    }
    
  
    
}
