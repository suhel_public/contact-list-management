
package com.suhel.contactmanagement.dal;

//import com.mysql.jdbc.Connection;
//import com.mysql.jdbc.Statement;
import com.suhel.contactmanagement.models.Contacts;
import com.suhel.contactmanagement.models.Users;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;

public abstract class AbstractDBLayer {
    protected Statement st;
    protected String dbhost;
    protected String dbname;
    protected String dbuser;
    protected String dbpass;
    protected String fullconnString="";
    protected Connection con=null;
    
   
  public abstract AbstractDBLayer getDBLayer();
  public abstract boolean userExists(Users user)throws Exception;
  public abstract boolean checkInsertContact(Contacts contact)throws Exception;
  public abstract ArrayList<Contacts> getAllContacts()throws Exception;
  public abstract void dbconnect();
  
  // for colsing the connection from DB 
  protected void closeConn()
    {
    try
    {
        if(con!=null)
        {
            con.close();
        }
    }catch(Exception e)
    {
       System.out.println(e.getMessage());
    }
    }
}
