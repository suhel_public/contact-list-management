/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suhel.contactmanagement.uis;
import static com.suhel.contactmanagement.ApplicationLauncher.stage;
import com.suhel.contactmanagement.controllers.InterfaceMainController;
import com.suhel.contactmanagement.models.Contacts;
import com.suhel.contactmanagement.models.Item;
import com.suhel.contactmanagement.uis.networks.ClientSide;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;


/**
 * FXML Controller class
 *
 * @author Md Suhel Rana
 */
public class ContactUIController implements Initializable,IContactUi {
      InterfaceMainController maincontroller;
      private ArrayList<Contacts>contacts=new ArrayList<Contacts>();
      ObservableList<Item> list=FXCollections.observableArrayList();
      final FileChooser fileChooser = new FileChooser();
      String line=null;
      
    @FXML
    private TextField txtfield_name,txtfield_phone,txtfield_email,txtfield_address,txtfield_city,txtfield_country;
   
    @FXML
    private Button btn_addcontact;
    
    @FXML
    private TableView<Item> tableview;
    @FXML
    private TableColumn<Item, String> columnPhone;
    @FXML
    private TableColumn<Item, String> columnName;
    @FXML
    private TextField txtsearch;
    
    // This method is used add contact to the list(database)
    @FXML
    private void addContactHandling(ActionEvent event) {
        
        String name=txtfield_name.getText();
        String phone=txtfield_phone.getText();
        String email=txtfield_email.getText();
        String address=txtfield_address.getText();
        String city=txtfield_city.getText();
        String country=txtfield_country.getText();
        maincontroller.addcontact(name, phone, email, address, city, country);
    }
    //This method is used to upadate contact 
     @FXML
    private void updateHandling(ActionEvent event) {
        //do update contact
    }
   // this method is used to delete contact from the list of contact
    @FXML
    private void deleteHandling(ActionEvent event) {
        // do delete contact
    }
  // This method is used to import contact from file to the list
    @FXML
    private void importHandling(ActionEvent event) {
        File file = fileChooser.showOpenDialog(stage);
                    if (file != null) {
                        openFile(file);
                    }
                }
    
    
  // This method is used to export contact to the file
    @FXML
    private void exportHandling(ActionEvent event) {
         // do Export contact
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }    

    public void setController(InterfaceMainController mc)
    {
        System.out.println("print set controller");
        this.maincontroller=mc;
        loadList();
        
         columnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
         columnPhone.setCellValueFactory(cellData -> cellData.getValue().phoneProperty());
        
        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<Item> filteredData = new FilteredList<>(list, c -> true);
        
        // 2. Set the filter Predicate whenever the filter changes.
        txtsearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(item -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (item.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (item.getPhone().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                return false; // Does not match.
            });
        });
        
        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<Item> sortedData = new SortedList<>(filteredData);
        
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tableview.comparatorProperty());
        
        // 5. Add sorted (and filtered) data to the table.
        tableview.setItems(sortedData);
    }  

    @Override
    public void addSuccess() {
        
        System.out.println("contact added successfully");
    }

    @Override
    public void addFailed() {
     System.out.println("contact added failed");

    }

    @Override
    public void errorMessageSent(String msg) {
        System.out.println(msg);
    }
    
    protected void loadList()
    {
        contacts= maincontroller.getContacts();
        for(int i=0; i<contacts.size();i++)
        {
              String name=contacts.get(i).getName();
              String phone=contacts.get(i).getPhone();
//            String email=contacts.get(i).getEmail();
//            String address=contacts.get(i).getAddress();
//            String city=contacts.get(i).getCity();
//            String country=contacts.get(i).getCountry();
              list.add(new Item(name,phone));
        }           
    }  

    private void openFile(File file) {
  
       
       //desktop.open(file);
       maincontroller.processFile(file);
       
    }

    @FXML
    private void openserver(ActionEvent event) {
        //ClientSide client=new ClientSide();
        //client.start();
    }
}
