
package com.suhel.contactmanagement.uis.networks;

import com.suhel.contactmanagement.controllers.MainController;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import org.apache.log4j.Logger;

public class ServerSide extends Thread {
     private Socket          socket   = null; 
    private ServerSocket    server   = null; 
    private DataInputStream in  =  null; 
    private static final Logger LOGGER=Logger.getLogger(ServerSide.class);
     @Override
  public void run(){  
  System.out.println("thread is running...");  
 
  handleServer();
 } 
    // constructor with port 
    public void handleServer() 
    { 
        // starts server and waits for a connection 
        try
        { 
            server = new ServerSocket(3000); 
            //System.out.println("Server started"); 
  
            System.out.println("Waiting for a client ..."); 
  
            socket = server.accept(); 
            System.out.println("Client accepted"); 
  
            // takes input from the client socket 
            in = new DataInputStream( 
                new BufferedInputStream(socket.getInputStream())); 
     System.out.println("get input channel");
            String line = ""; 
  
            // reads message from client until "Over" is sent 

                try
                { 
                    line = in.readUTF(); 
                    LOGGER.info("message from client "+line); 
  
                } 
                catch(IOException ex) 
                { 
                    System.out.println(ex); 
                } 
             
           
  
            // close connection 
            socket.close(); 
            in.close(); 
        } 
        catch(IOException i) 
        { 
            System.out.println(i); 
        } 
    } 
}
