
package com.suhel.contactmanagement.uis;

import com.suhel.contactmanagement.ApplicationLauncher;
import com.suhel.contactmanagement.controllers.InterfaceMainController;
import com.suhel.contactmanagement.controllers.MainController;
import com.suhel.contactmanagement.utilities.PropertyReader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


public class LoginUiController implements Initializable,ILoginUi {
    InterfaceMainController mc=null;
     private String userid;
     private String password;

    @FXML
    private TextField txt_userid;
    
    @FXML 
    private TextField txt_password;
    
    @FXML
    private Label label_show;
    @FXML
    private Button btn_submit;
    @FXML
    private ImageView iv1;
    
    @FXML
    private void submitAction(ActionEvent event)
    {
        userid=txt_userid.getText();
        password= txt_password.getText();
        mc.checkLogin(userid, password);
    }
    private void loadContactUi(){    
        try {
                                           
        FXMLLoader loader=new FXMLLoader(getClass().getResource("contact_ui.fxml"));
        System.out.println("Calling schene method");
        Parent root = loader.load();
        Stage stage=new Stage();
          
        Scene scene = new Scene(root);  
        stage.setScene(scene);
        ContactUIController contactuiController=
                loader.<ContactUIController>getController();
        contactuiController.setController(mc);
        mc.setContactUi(contactuiController);
        
        ApplicationLauncher.stage.close();
        stage.show();
                
            } catch (IOException ex) {
               System.out.println(ex.getMessage());
            }
       }
     
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      loadImage();
    }    
    
    public void setController(InterfaceMainController mc)
    {
        this.mc=mc;
    }

    @Override
    public void showErrorMessage(String msg) {
        System.out.println("Error ======="+msg);
    }

    @Override
    public void loginSuccess() {
        System.out.println("login success");
        loadContactUi();
    }

    @Override
    public void loginFailed()
    {
        System.out.println("wrong userid and password");
        label_show.setText(userid+" & "+password);
    }

    private void loadImage() {
   // File file = new File("C:\\Users\\Md Suhel Rana\\Desktop\\logos\\logo1.jpg");
    
        try {
            String path=PropertyReader.getProperty("config.properties", "imageUrl");
            File file=new File(path);
            Image image = new Image(file.toURI().toString());
            iv1.setImage(image);
            iv1.setSmooth(true);
            iv1.setPreserveRatio(true);
        } catch (IOException ex) {
           System.out.println(ex.getMessage());
        }
    //Image image = new Image("images/logo.jpg");  
    
    }
}
