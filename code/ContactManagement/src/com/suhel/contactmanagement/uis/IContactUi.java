
package com.suhel.contactmanagement.uis;

import com.suhel.contactmanagement.models.Contacts;
import java.util.ArrayList;


public interface IContactUi {
   void addSuccess();
   void addFailed();
   void errorMessageSent(String msg);
}
