
package com.suhel.contactmanagement.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Item {
   private final StringProperty name;
   private StringProperty phone;
        

    public Item(String name, String phone) {
        this.name = new SimpleStringProperty(name);
       this.phone = new SimpleStringProperty(phone);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getPhone() {
        return phone.get();
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }
    public StringProperty nameProperty()
    {
        return name;
    }
    public StringProperty phoneProperty()
    {
        return phone;
    }  
}
